import smtplib
import ssl
import json
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
import os
import sys
import shutil
from os.path import join, isfile
import datetime


now = datetime.datetime.now()
GMAIL = 'email_account'
PASSWORD = 'passwd'
ADMINISTRATOR_EMAIL = 'corporate_account'

client = sys.argv[1]

json_file = open("clients.json", "r")
list_clients = json.load(json_file)

if not client in list_clients.keys():
    print("This client does not exist.")
    sys.exit(1)

dir_path = os.getcwd()
dir_path = os.path.dirname(dir_path)
work_in_progress_path = os.path.join(dir_path, 'WIP')
end_pdf_destination_path = os.path.join(
    dir_path, 'Done/Facturas_' + str(now.year))
end_numbers_destination_path = os.path.join(dir_path, 'Done/original_files')

def get_files_from_dir():
    global work_in_progress_path
    list_files = []
    for path in os.listdir(work_in_progress_path):
        path_file = os.path.join(work_in_progress_path, path)
        if os.path.isfile(path_file):
            list_files.append(path_file)
    return list_files


def archive_files():
    global end_pdf_destination_path
    list_files = get_files_from_dir()
    for file in list_files:
        filename = file.split("/")[-1]
        file_extension = file.split("/")[-1].split(".")[-1]
        if file_extension == 'numbers':
            shutil.move(file, os.path.join(
                end_numbers_destination_path, filename))


def get_client_invoices():
    global client
    global check_multiple_files
    archive_files()
    list_files = get_files_from_dir()
    client_invoices = []
    for invoice in list_files:
        if client in invoice:
            client_invoices.append(invoice)
    if not client_invoices:
        print("No invoices availabes of {}".format(client))
        sys.exit(1)

    return client_invoices


def get_corporate_file_name(invoice_file):
    original_file_name = invoice_file.split("/")[-1].split("-")
    del original_file_name[-2:]
    original_file_name.insert(0, '2022-')
    original_file_name.append(" corporate-name.pdf")
    original_file_name = ''.join(original_file_name)
    return original_file_name


def add_attachment():
    global end_pdf_destination_path
    pdf_files_to_send = get_client_invoices()
    for file in pdf_files_to_send:
        with open(file, "rb") as attachment:
            print(get_corporate_file_name(file))
            attachment = MIMEApplication(attachment.read(), _subtype="pdf")
            attachment.add_header(
                'Content-Disposition', 'attachment', filename=get_corporate_file_name(file))
            shutil.move(file, os.path.join(
                end_pdf_destination_path, file.split("/")[-1]))
            msg_mixed.attach(attachment)


if len(get_client_invoices()) <= 1:
    html = "<html>Template message for one invoice</p></html>".format(
        client.capitalize())
else:
    html = "<html>Template message for multiple invoices</p></html>".format(
        client.capitalize())


html_part = MIMEText(html, 'html')
msg_alternative = MIMEMultipart('alternative')
msg_alternative.attach(html_part)

msg_mixed = MIMEMultipart('mixed')
msg_mixed.attach(msg_alternative)
add_attachment()

msg_mixed['From'] = GMAIL
msg_mixed['To'] = list_clients[client]
msg_mixed['Cc'] = ADMINISTRATOR_EMAIL
msg_mixed['Subject'] = 'New invoice from corporate-name'

receiver_email = [list_clients[client], ADMINISTRATOR_EMAIL]
smtp_server = 'smtp.gmail.com'
smtp_port = 587

try:
    context = ssl.create_default_context()
    server = smtplib.SMTP(smtp_server, smtp_port)
    server.ehlo()
    server.starttls(context=context)
    server.ehlo()
    server.login(GMAIL, PASSWORD)
    server.sendmail(msg_mixed['From'], receiver_email, msg_mixed.as_string())
    server.set_debuglevel(1)
    print("email sent out successfully")
except Exception as err:
    # Print any error messages to stdout
    print("Error!!")
    print(err)
finally:
    server.quit()
